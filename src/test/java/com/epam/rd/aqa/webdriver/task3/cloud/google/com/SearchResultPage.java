package com.epam.rd.aqa.webdriver.task3.cloud.google.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.InvocationTargetException;

public class SearchResultPage {

    private final WebDriver driver;

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public <T> T followResultLink(String linkText, Class<T> expectedPageObjectClass) {
        driver.findElement(By.linkText(linkText)).click();
        try {
            return expectedPageObjectClass.getConstructor(WebDriver.class).newInstance(driver);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
