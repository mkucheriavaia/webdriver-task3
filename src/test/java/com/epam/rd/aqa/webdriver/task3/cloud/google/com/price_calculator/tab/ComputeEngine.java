package com.epam.rd.aqa.webdriver.task3.cloud.google.com.price_calculator.tab;

import com.epam.rd.aqa.webdriver.task3.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class ComputeEngine {

    public static final String OPTIONS_XPATH_TEMPLATE = "//div[@id='%s']//md-option[normalize-space()='%s']";

    private static final String FORM_DROPDOWN_ELEMENT_LOCATOR_TEMPLATE = "//md-select[@ng-model='%s']";
    private static final String OPERATING_SYSTEM = "listingCtrl.computeServer.os";
    public static final String PROVISIONING_MODEL = "listingCtrl.computeServer.class";
    public static final String MACHINE_FAMILY = "listingCtrl.computeServer.family";
    public static final String SERIES = "listingCtrl.computeServer.series";
    public static final String MACHINE_TYPE = "listingCtrl.computeServer.instance";
    public static final String GPU_TYPE = "listingCtrl.computeServer.gpuType";
    public static final String GPU_COUNT = "listingCtrl.computeServer.gpuCount";
    public static final String LOCAL_SSD = "listingCtrl.computeServer.ssd";
    public static final String DATA_CENTER_LOCATION = "listingCtrl.computeServer.location";
    public static final String COMMITTED_USAGE = "listingCtrl.computeServer.cud";

    @FindBy(xpath = "//input[@ng-model='listingCtrl.computeServer.quantity']")
    private WebElement numberOfInstancesInput;

    @FindBy(xpath = "//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']")
    private WebElement addGPUCheckbox;

    @FindBy(xpath = "//button[@ng-click='listingCtrl.addComputeServer(ComputeEngineForm);'][not(@disabled)]")
    private WebElement instanceAddToEstimateButton;

    @FindBy(css = "div.cpc-cart-total")
    private WebElement calcTotalText;

    @FindBy(xpath = "//button[@id='Email Estimate']")
    private WebElement emailEstimateButton;

    private final WebDriver driver;

    public ComputeEngine(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public ComputeEngine fillOutForm(int numberOfInstances, String operatingSystem, String provisioningModel,
                                     String machineFamily, String series, String machineType,
                                     String gpuType, int gpuCount, String localSSD, String datacenterLocation,
                                     String committedUsage) {
        this.numberOfInstancesInput.sendKeys(String.valueOf(numberOfInstances));

        selectFromDropDown(OPERATING_SYSTEM, operatingSystem);
        selectFromDropDown(PROVISIONING_MODEL, provisioningModel);
        selectFromDropDown(MACHINE_FAMILY, machineFamily);
        selectFromDropDown(SERIES, series);
        selectFromDropDown(MACHINE_TYPE, machineType);

        addGPUCheckbox.click();

        selectFromDropDown(GPU_TYPE, gpuType);
        selectFromDropDown(GPU_COUNT, String.valueOf(gpuCount));
        selectFromDropDown(LOCAL_SSD, localSSD);
        selectFromDropDown(DATA_CENTER_LOCATION, datacenterLocation);
        selectFromDropDown(COMMITTED_USAGE, committedUsage);

        return this;
    }

    private void selectFromDropDown(String ngModel, String text) {
        WebElement element = driver.findElement(By.xpath(String.format(FORM_DROPDOWN_ELEMENT_LOCATOR_TEMPLATE, ngModel)));
        element.click();
        new Actions(driver).scrollByAmount(0, 50).perform();
        Optional.ofNullable(
                new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                        .until(ExpectedConditions.elementToBeClickable(
                                element.findElement(By.xpath(String.format(OPTIONS_XPATH_TEMPLATE,
                                        element.getAttribute("aria-owns"), text))))))
                .ifPresent(WebElement::click);
    }

    public ComputeEngine clickInstanceAddToEstimate() {
        instanceAddToEstimateButton.click();
        return this;
    }

    public String getTotalEstimate() {
        return calcTotalText.getText().trim();
    }

    public ComputeEngine clickEmailEstimate() {
        emailEstimateButton.click();
        return this;
    }

    public <T extends Page<T>> T switchToNewTab(Class<T> expectedPageObjectClass) {
        driver.switchTo().newWindow(WindowType.TAB);
        try {
            return expectedPageObjectClass.getConstructor(WebDriver.class).newInstance(driver).openPage();
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
