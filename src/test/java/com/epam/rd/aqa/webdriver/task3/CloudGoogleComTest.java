package com.epam.rd.aqa.webdriver.task3;

import com.epam.rd.aqa.webdriver.task3.cloud.google.com.HomePage;
import com.epam.rd.aqa.webdriver.task3.cloud.google.com.PricingCalculatorPage;
import com.epam.rd.aqa.webdriver.task3.cloud.google.com.price_calculator.tab.ComputeEngine;
import com.epam.rd.aqa.webdriver.task3.yopmail.com.EmailInbox;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CloudGoogleComTest {

    private WebDriver driver;

    @BeforeAll
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterAll
    public void cleanUp() {
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle).close();
        }
        driver.quit();
    }

    @Test
    public void testCloudGoogleCom() {

        ComputeEngine computeEngineTab = new HomePage(driver)
                .openPage()
                .searchFor("Google Cloud Platform Pricing Calculator")
                .followResultLink("Google Cloud Pricing Calculator", PricingCalculatorPage.class)
                .selectTab("Compute Engine", ComputeEngine.class)
                .fillOutForm(4, "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)",
                        "Regular", "General purpose", "N1",
                        "n1-standard-8 (vCPUs: 8, RAM: 30GB)", "NVIDIA Tesla T4", 1, "2x375 GB",
                        "Frankfurt (europe-west3)", "1 Year")
                .clickInstanceAddToEstimate()
                .clickEmailEstimate();

        String actualTotalEstimate = computeEngineTab.getTotalEstimate();
        Matcher matcher = Pattern.compile("Total Estimated Cost: (USD [\\d.,]+)\\s+per 1 month")
                .matcher(actualTotalEstimate);
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(matcher.matches()).isTrue();

        EmailInbox emailInbox = computeEngineTab
                .switchToNewTab(com.epam.rd.aqa.webdriver.task3.yopmail.com.HomePage.class)
                .generateRandomEmail();

        String generatedEmail = emailInbox.getGeneratedEmail();

        emailInbox = emailInbox.switchBackToPriceCalculator()
                .sendEmailWithEstimation(generatedEmail)
                .switchToEmailTab()
                .waitForEstimationEmail();
        String emailBodyText = emailInbox.getEmailBodyText();
        softly.assertThat(Pattern.matches("(?s)^.*Total Estimated Monthly Cost\\s+" + matcher.group(1) + "\\s*\\n.*$", emailBodyText)).isTrue();
        softly.assertAll();
    }
}