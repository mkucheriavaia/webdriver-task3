package com.epam.rd.aqa.webdriver.task3;

public interface Page<T> {
    T openPage();
}
